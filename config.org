#+TITLE: Emacs Configuration
#+AUTHOR: Brad Stinson
This is my emacs initialization file. This file (config.org) is an org mode
file that is parsed by Emacs and exported into a config.el file containing nothing
but the Emacs-lisp code found within this file. It is config.el that Emacs evaluates
on startup to carry out the desired functionality.

* Main Config
** Personal Information
#+begin_src emacs-lisp
(setq user-full-name "Brad Stinson"
      user-mail-address "brad@bradthebuilder.me")
#+end_src
** Customize settings

Set up the customize file to its own separate file, instead of saving
customize settings in [[file:init.el][init.el]]. 

#+begin_src emacs-lisp
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file)
#+end_src

** Sources
#+BEGIN_SRC emacs-lisp
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-refresh-contents)
#+END_SRC

Allow Emacs to auto-load packages (recursively) found in lisp folder. Used for Github sourced packages not available through melpa. Since some packages are more at risk of being orphaned than others, it makes sense to delineate across your configuration what packages are sourced from melpa and which are sourced externally.

Moreover, it is a good practice to note when and how packages depend on the presence of OS-specific dependencies.

#+BEGIN_SRC emacs-lisp
(let ((default-directory  "~/.emacs.d/lisp/"))
  (normal-top-level-add-subdirs-to-load-path))
#+END_SRC

** Sane defaults
Sources for this section include [[https://github.com/magnars/.emacs.d/blob/master/settings/sane-defaults.el][Magnars Sveen]] and [[http://pages.sachachua.com/.emacs.d/Sacha.html][Sacha Chua]].

#+begin_src emacs-lisp
;; These functions are useful. Activate them.
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)

;; Answering just 'y' or 'n' will do
(defalias 'yes-or-no-p 'y-or-n-p)

;; Keep all backup and auto-save files in one directory
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t)))

;; UTF-8 please
(setq locale-coding-system 'utf-8) ; pretty
(set-terminal-coding-system 'utf-8) ; pretty
(set-keyboard-coding-system 'utf-8) ; pretty
(set-selection-coding-system 'utf-8) ; please
(prefer-coding-system 'utf-8) ; with sugar on top
(setq-default indent-tabs-mode nil)

;; Turn off the blinking cursor
(blink-cursor-mode -1)

(setq-default indent-tabs-mode nil)
(setq-default indicate-empty-lines t)

;; Don't count two spaces after a period as the end of a sentence.
;; Just one space is needed.
(setq sentence-end-double-space nil)

;; delete the region when typing, just like as we expect nowadays.
(delete-selection-mode t)

(show-paren-mode t)

(column-number-mode t)

(global-visual-line-mode)
(diminish 'visual-line-mode)

(setq uniquify-buffer-name-style 'forward)

;; -i gets alias definitions from .bash_profile
;(setq shell-command-switch "-ic")

;; Don't beep at me
(setq visible-bell t)
#+end_src

The following function for ~occur-dwim~ is taken from [[https://github.com/abo-abo][Oleh Krehel]] from
[[http://oremacs.com/2015/01/26/occur-dwim/][his blog post at (or emacs]]. It takes the current region or the symbol
at point as the default value for occur.

#+begin_src emacs-lisp
(defun occur-dwim ()
  "Call `occur' with a sane default."
  (interactive)
  (push (if (region-active-p)
            (buffer-substring-no-properties
             (region-beginning)
             (region-end))
          (thing-at-point 'symbol))
        regexp-history)
  (call-interactively 'occur))

  (bind-key "M-s o" 'occur-dwim)
#+end_src
** Aesthetic

Set theme and ensure proper text color contrast

#+BEGIN_SRC emacs-lisp
(load-theme 'wombat)
(setq frame-background-mode "dark")
(custom-set-faces '(term-color-blue ((t (:foreground "#00bfff" :background "#00bfff")))))
#+END_SRC
*** Org Mode
#+BEGIN_SRC emacs-lisp
(setq org-startup-indented t)
#+END_SRC
*** Nyan Mode

Programming is fun, and Nyan Cat is there to remind me of that.

#+BEGIN_SRC emacs-lisp
(use-package nyan-mode
  :ensure t
  :init
  (nyan-mode)
)
#+END_SRC
* Development Config
** Universal
*** magit
#+BEGIN_SRC emacs-lisp
(use-package magit
  :ensure t
)
(use-package ssh-agency
  :ensure t
)
(global-set-key (kbd "C-x g") 'magit-status)
(setq ssh-agency-add-executable "/usr/bin/ssh-add")
(setq ssh-agency-agent-executable "/usr/bin/ssh-agent")
(setenv "SSH_ASKPASS" "git-gui--askpass")
#+END_SRC
*** iedit
#+BEGIN_SRC emacs-lisp
(use-package iedit
  :ensure t
)
(define-key global-map (kbd "C-c ;") 'iedit-mode)
#+END_SRC
*** multi-term
#+BEGIN_SRC emacs-lisp
(use-package multi-term
  :ensure t
)
(setq multi-term-program "/bin/zsh")
(add-hook 'term-mode-hook
          (lambda ()
            (setq term-buffer-maximum-size 1000)))
(add-hook 'term-mode-hook
          (lambda ()
            (add-to-list 'term-bind-key-alist '("M-[" . multi-term-prev))
            (add-to-list 'term-bind-key-alist '("M-]" . multi-term-next))))
(global-set-key (kbd "C-c t") 'multi-term)
            ;Adding 'term-line-mode as a hook does not work. Keybinding the mode also fails. =/
            ;These 2 functions used to work, but no longer. I have to manually switch to term-line-mode
            ;in order to get access to Emacs keybindings (including mark and kill), then return to
            ;term-char-mode
            ;(add-to-list 'term-bind-key-alist '("C-<SPC>" . set-mark-command))
            ;(add-to-list 'term-bind-key-alist '("M-w" . kill-ring-save))))
#+END_SRC
*** projectile
#+BEGIN_SRC emacs-lisp
(use-package projectile
  :ensure t
)
#+END_SRC
*** flycheck-mode
#+BEGIN_SRC emacs-lisp
(use-package flycheck
  :ensure t
)
#+END_SRC
*** macrostep
#+BEGIN_SRC emacs-lisp
(use-package macrostep
  :ensure t
)
#+END_SRC
** Python
*** elpy

Elpy is a python IDE mode that combines other Emacs modes. Elpy requires:
#1 Python installed
#2 Pip installed (Pip is a python module for installing other python modules)
#3 Elpy installed via pip (python-code implementation of elpy that connects to Emacs elpy package)
#4 Rope and/or Jedi backends installed via pip (more python code for autocompletion)
#5 Elpy emacs package installed

#+BEGIN_SRC emacs-lisp
(use-package elpy
  :ensure t
)
(elpy-enable)
(define-key yas-minor-mode-map (kbd "C-c k") 'yas-expand)
#+END_SRC
*** virtualenvwrapper
#+BEGIN_SRC emacs-lisp
(use-package virtualenvwrapper
  :ensure t
)
(venv-initialize-eshell)
(venv-initialize-interactive-shells)
;(setq venv-location (getenv "WORKON_HOME"))
(setq venv-location (shell-command-to-string ". ~/.zshrc; echo -n $WORKON_HOME"))
;(setq venv-project-home (getenv "PROJECT_HOME"))
(setq venv-project-home (shell-command-to-string ". ~/.zshrc; echo -n $PROJECT_HOME"))
(setenv "WORKON_HOME" venv-location)
(setenv "PROJECT_HOME" venv-project-home)
(setq-default mode-line-format (cons '(:exec venv-current-name) mode-line-format))
(setq eshell-prompt-function
 (lambda ()
   (concat "(" venv-current-name ")" (eshell/pwd) " $ ")))
(add-hook 'venv-postactivate-hook
  (lambda () (projectile-mode)))
(add-hook 'venv-postactivate-hook
  (lambda () (eshell/cd (concat venv-project-home "/" venv-current-name))))
#+END_SRC
** Ruby
** C++
** Elisp
#+BEGIN_SRC emacs-lisp
(use-package paredit
  :ensure t
  :config
  (add-hook 'emacs-lisp-mode-hook #'paredit-mode)
  ;; enable in the *scratch* buffer
  (add-hook 'lisp-interaction-mode-hook #'paredit-mode)
  (add-hook 'ielm-mode-hook #'paredit-mode)
  (add-hook 'lisp-mode-hook #'paredit-mode)
  (add-hook 'eval-expression-minibuffer-setup-hook #'paredit-mode)
  (add-hook 'scheme-mode-hook #'enable-paredit-mode)
)
(add-hook 'after-save-hook 'check-parens nil t)
#+END_SRC
* Cloud/Web/Text
** HTML mode
#+BEGIN_SRC emacs-lisp
(add-hook 'html-mode-hook 'yas-minor-mode)
 #+END_SRC
** Yaml mode
#+BEGIN_SRC emacs-lisp
(use-package yaml-mode
  :ensure t
)
(add-to-list 'load-path "~/.emacs.d/lisp/")
(load "highlight-indentation")
(add-hook 'yaml-mode-hook 'highlight-indentation-mode)
(add-hook 'yaml-mode-hook 'flycheck-mode)
(package-initialize)
;; SmartShift requires manual installation
(require 'smart-shift)
(global-smart-shift-mode 1)

(defun aj-toggle-fold ()
  "Toggle fold all lines larger than indentation on current line"
  (interactive)
  (let ((col 1))
    (save-excursion
      (back-to-indentation)
      (setq col (+ 1 (current-column)))
      (set-selective-display
       (if selective-display nil (or col 1))))))
(global-set-key [(C M f)] 'aj-toggle-fold)
#+END_SRC
** markdown mode
#+BEGIN_SRC emacs-lisp
(use-package markdown-mode
  :ensure t
)
#+END_SRC
** LateX
* Misc
** ESV Mode

ESV Mode must currently be manually installed from GitHub. The original author has orphaned the project.
The most likely new maintainer is [[https://github.com/emacsmirror/esv][hosted at this fork.]]

#+BEGIN_SRC emacs-lisp
(require 'esv)
  ; C-c e looks up a passage and displays it in a pop-up window
  (define-key global-map [(control c) ?e] 'esv-passage)
  ; C-c i inserts an ESV passage in plain-text format at point
  (define-key global-map [(control c) ?i] 'esv-insert-pa)
  (with-temp-buffer
  (insert-file-contents "~/.emacs.d/lisp/esvToken.txt")
  (setq esv-key (when (string-match "\\(.*\\)" (buffer-string))
          (match-string 1 (buffer-string)))))
#+END_SRC
** Keybindings
#+BEGIN_SRC emacs-lisp
(define-prefix-command 'lisp-functional-map)
(global-set-key (kbd "C-c e") 'lisp-functional-map)
(global-set-key (kbd "C-c e b") 'eval-buffer)
(global-set-key (kbd "C-c e e") 'toggle-debug-on-error)
(global-set-key (kbd "C-c e f") 'emacs-lisp-byte-compile-and-load)
(global-set-key (kbd "C-c e r") 'eval-region)

(define-prefix-command 'lisp-find-map)
(global-set-key (kbd "C-h e") 'lisp-find-map)

(global-set-key (kbd "C-h e e") 'view-echo-area-messages)
(global-set-key (kbd "C-h e f") 'find-function)
(global-set-key (kbd "C-h e k") 'find-function-on-key)
(global-set-key (kbd "C-h e l") 'find-library)
(global-set-key (kbd "C-h e v") 'find-variable)
(global-set-key (kbd "C-h e V") 'apropos-value)

(global-set-key (kbd "C-c e m") 'macrostep-expand)
#+END_SRC
