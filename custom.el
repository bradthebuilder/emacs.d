(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(initial-frame-alist (quote ((fullscreen . maximized))))
 '(package-selected-packages
   (quote
    (macrostep paredit yasnippet-snippets yaml-mode virtualenvwrapper use-package ssh-agency projectile nyan-mode multi-term markdown-mode magit iedit flycheck elpy diminish))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(highlight-indentation-face ((t (:inherit fringe :background "dark gray"))))
 '(term-color-blue ((t (:foreground "#00bfff" :background "#00bfff")))))
